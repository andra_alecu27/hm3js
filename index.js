
const FIRST_NAME = "Andrada";
const LAST_NAME = "Alecu";
const GRUPA = "";

/**
 * Make the implementation here
 */
class Employee {
   
    constructor(name,surname,salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails(){
        return this.name+" "+this.surname+" "+this.salary;
    }
    
}

class SoftwareEngineer extends Employee{
   
    constructor(name,surname,salary,experience='JUNIOR'){
        super(name,surname,salary);
        this.experience=experience;
    }
    applyBonus(){
        let salary;
        switch(this.experience){
            case 'JUNIOR':
                   salary=this.salary*1.1;
                 break;
             case 'MIDDLE':
                salary= this.salary*1.15; 
                 break;
            case 'SENIOR':
                    salary = this.salary*1.2;
                 break;  
            default:     
                   salary= this.salary*1.1;
                 break;     


        }
                     return salary;
    
    }
   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

